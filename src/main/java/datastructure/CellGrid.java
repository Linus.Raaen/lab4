package datastructure;

import java.lang.reflect.Array;
import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private CellState cellstatus[][];
    
    

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.cols = columns;
        cellstatus = new CellState[rows][cols];
        int i = 0;
        int j = 0;
        while(i<rows){
            while (j<columns){
                cellstatus[i][j] = initialState;
                j++;
            }
            i++;}
}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        
        if (row>=0 && row<rows){
            
            if (column>=0 && column < cols){
                
                cellstatus[row][column] = element;
            } else { throw new IndexOutOfBoundsException("Invalid column for cell");
        }
        } else { throw new IndexOutOfBoundsException("Invalid row for cell");

        }
        
    
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        
        if (row>=0 && row<rows){
            
            if (column>=0 && column < cols){
                
                return cellstatus[row][column];
            } else { throw new IndexOutOfBoundsException("Invalid column for cell. Kolonneverdi: " +column+" maks antall kolonner: " + numColumns());
        }} else { throw new IndexOutOfBoundsException("Invalid row for cell");

        }
        
   
        
    }


    @Override
    public IGrid copy() {
        IGrid celleGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
        for (int i = 0; i<this.rows; i++){
            for (int j=0;j<this.cols;j++){
                celleGrid.set(i, j,  this.get(i, j));
                }
        }
        return celleGrid;
        
    }
    
}
